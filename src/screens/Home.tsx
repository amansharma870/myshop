import * as React from 'react'
import {SafeAreaView, ScrollView, Text, View} from 'react-native';
import CataSlider from '../components/CataSlider';
import Container from '../components/Container';
import TopSlider from '../components/TopSlider';
import { MainSlider } from '../styles/SliderData';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import { utils } from '../utils/utils';
import SimSlider from '../components/SimpleSlider';
import Footer from './Footer';
import { Typography } from '../styles/Global';
import { PrimaryTheme } from '../styles/Theme';
import CustomButton from '../components/CustomButton';
import RowOpt from '../components/RowOptions';
import WDContent from '../components/Wd-content';
import OfferCnt from '../components/Offers';

interface Props{
    route:any
    navigation:any
  }
const Home =(props:Props)=>
    {
        return(
            <SafeAreaView style={{}}>
                <ScrollView showsVerticalScrollIndicator={false}>
                <TopSlider 
                onPress={{}}
                images={MainSlider[0]}
                SliderImage={{}}

                />
                <Container>
                  <CataSlider 
                  onPress={{}}
                  icons={MainSlider[1]}
                  SliderImage={{}}
                  />
                  </Container>
                  <OfferCnt need={'BgColor'}/>
                  <Container>
                  <RowOpt rowTitle={'Best Seller'}/>
                  <View style={{flexDirection:'row'}}>
                    <WDContent 
                        wdImg={utils.Img.WDP1}
                        productName={'Elegant Blue t-Shirt'}
                        productPrice={'$810.00'}/>
                    <WDContent 
                        wdImg={utils.Img.WDP2}
                        productName={'Elegant Blue t-Shirt'}
                        productPrice={'$20.00'}/>
                   </View>
                   <View style={{flexDirection:'row'}}>
                    <WDContent 
                        wdImg={utils.Img.WDP3}
                        productName={'Elegant Blue t-Shirt'}
                        productPrice={'$810.00'}/>
                    <WDContent 
                        wdImg={utils.Img.WDP4}
                        productName={'Elegant Blue t-Shirt'}
                        productPrice={'$20.00'}/>
                   </View>
                </Container>
            
                <Container>
                    <SimSlider source={MainSlider[2]}/>
                    <SimSlider source={MainSlider[3]}/>
                </Container>
                <Footer />
               
                </ScrollView>
            </SafeAreaView>
        )
    }
export default Home;