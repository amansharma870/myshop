import React from 'react'
import { View,Text ,StyleSheet, ViewStyle, TextStyle, ImageStyle, FlatList,TouchableOpacity} from "react-native"
import ImageOverlay from "react-native-image-overlay";
import { Typography } from '../styles/Global';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import * as Animatable from 'react-native-animatable';
import CustomButton from './CustomButton';
import { PrimaryTheme } from '../styles/Theme';


interface Props{
    onPress?: any;
    subPlace?: any;
    subHeading?: any;
    overlayAlpha?: any;
    overlayColor?: any;
    contentContainerCustomStyle?:any
        containerStyle?:ViewStyle |   ViewStyle[]
    |   TextStyle
    |   TextStyle[]
    |   ImageStyle
    |   ImageStyle[],
    SliderImage:any,
    images:any
}

const TopSlider = (props:Props) => {
    return (
        <View style={{flexDirection: 'row', 
        flexWrap: 'wrap',marginBottom:hp('2%')}}>
<FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={props.images}
        renderItem={({ item, index }) => (
            <TouchableOpacity style={{flexDirection: 'row', 
            flexWrap: 'wrap',paddingRight:wp('3%')}} onPress={props.onPress}activeOpacity={0.9} key={index}>

                <ImageOverlay
                source={item.image}
                containerStyle={[styles.image,props.SliderImage]}
                overlayColor={props.overlayColor}
                overlayAlpha={props.overlayAlpha}
                >
                   <Animatable.View animation={'fadeInDownBig'}>
                        <Text  style={[Typography.paragraph,{color:"#fff",textAlign:"center"}]}>{item.title}</Text>
                        <Text style={[Typography.paragraph,{color:"#fff",fontSize: 20,fontWeight:'bold'}]}>{item.subTitle}</Text>
                        <Text style={[Typography.paragraph,{color:"#fff",textAlign:"center"}]}>{item.Paragraph}</Text>
                        <CustomButton title={'Contact Us'} onPress={{}} buttonStyle={{backgroundColor:PrimaryTheme.$MATERIAL_COLOR,width:wp('40%'),height:hp('5%'),alignSelf:'center'}}/>
                    </Animatable.View>
                </ImageOverlay>
                </TouchableOpacity>
        )}
        keyExtractor={(item,index) => index.toString()}

      />
    </View>
    )
}
export default TopSlider;

const styles = StyleSheet.create({

image: {
    
},

})