import React from 'react';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import {
  Text,
  TextStyle,
  TouchableOpacity,
  ViewStyle,
  StyleSheet,
} from 'react-native';
import { PrimaryTheme } from '../styles/Theme';


export interface Props {
  title: string;
  disabled?: boolean;
  buttonStyle?: ViewStyle | ViewStyle[];
  textStyle?: TextStyle | TextStyle[];
  onPress: any;
}

const CustomButton = (props: Props) => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={[
        props.disabled ? {...styles.buttonStyle,backgroundColor:'#000'}
       : styles.buttonStyle, props.buttonStyle,
      props.buttonStyle,
    
    ]}
      disabled={props.disabled}>
      
      <Text style={[styles.textStyle, props.textStyle]}>{props.title}</Text>
    </TouchableOpacity>
  );
};
CustomButton.defaultProps = {
  disabled: false,
  IconSize:30,
  IconColor:'#fff',
};
const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor:PrimaryTheme.$MATERIAL_COLOR,
    borderRadius: 5,
    borderWidth:1,
    borderColor:'#fff',
    height:hp('7%'),
    alignItems:'center',
    flexDirection:'row',
    justifyContent:'center',
    marginTop:hp('1%'),
  },
  textStyle: {
    color: PrimaryTheme.$LIGHT_COLOR,
    fontSize:18
  },
});
export default CustomButton;
