import { utils } from "../utils/utils";

export const MainSlider= [
    [
        { id: 1, image: (utils.Img.WDP1), title: "Welcome to Jazy", subTitle:"LOOK LIKE PROFFESIONAL",Paragraph:"25% OFF on Jazy Sale"},
        { id: 2, image: (utils.Img.WDP2),title: "Treditional Guchi", subTitle:"TRADITIONAL TWIN FESTIVAL",Paragraph:"30% OFF on Treditional Guchi Sale"},
        { id: 3, image: (utils.Img.WDP5),title: "Proffesional Armani", subTitle:"PRIDE PROFFESIONAL CUTS",Paragraph:"10% OFF on Proffesional Armani"},
        { id: 4, image: (utils.Img.WDP6),title: "Own Style of Tommy", subTitle:"STYLES MADE FOR YOU",Paragraph:"20% OFF on Tommy Sale"},
    ],
    [
        { id: 1, name: 'phone-portrait', title: "Phone"},
        { id: 2, name: 'shirt',title: "T-Shirt"},
        { id: 3, name: 'tennisball',title: "Sports"},
        { id: 4, name: 'game-controller',title: "Tools"},
        { id: 4, name: 'chevron-forward',title: "See All"},
    ],
    [
        { id: 1, image: (utils.Img.HSP1)},
        { id: 2, image: (utils.Img.HSP2)},
        { id: 3, image: (utils.Img.HSP3)},
        { id: 4, image: (utils.Img.HSP4)},
    ],
    [
        { id: 1, image: (utils.Img.HSPA1)},
        { id: 2, image: (utils.Img.HSPA2)},
        { id: 3, image: (utils.Img.HSPA3)},
        { id: 4, image: (utils.Img.HSPA4)},
    ],
    [
        { id: 1, name: 'logo-facebook'},
        { id: 2, name: 'logo-twitter'},
        { id: 3, name: 'logo-google'},
        { id: 4, name: 'logo-instagram'},
    ],
    [
        { id: 1, image: (utils.Img.OFB1)},
        { id: 2, image: (utils.Img.OFB2)},
        { id: 3, image: (utils.Img.OFB3)},
        { id: 1, image: (utils.Img.OFB1)},
        { id: 2, image: (utils.Img.OFB2)},
    ],

]
