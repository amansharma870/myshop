import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Dashboard from '../../screens/Dashboard';


const Stack=createStackNavigator();
export enum ScreenName {
  DASHBOARD='Dashboard',
}

export const authStack=()=>{
    return(
        <Stack.Navigator initialRouteName={ScreenName.DASHBOARD}>
          <Stack.Screen options= {{headerShown: false}} name={ScreenName.DASHBOARD} component={Dashboard}/>
        </Stack.Navigator>
    )
}
