import  React from 'react';
import { ImageStyle, TextStyle, ViewStyle ,Platform} from 'react-native';
  export class utils{
        static image: any;
        static isIos() {
            throw new Error('Method not implemented.');
        }
        static Img={
            WDP1:require('../assets/Images/product1.png'),
            WDP2:require('../assets/Images/product2.png'),
            WDP3:require('../assets/Images/product3.png'),
            WDP4:require('../assets/Images/product4.png'),
            WDP5:require('../assets/Images/product5.png'),
            WDP6:require('../assets/Images/product6.png'),
            HSBanner:require('../assets/Images/HSBanner.jpg'),
            HSP1:require('../assets/Images/hsproduct1.jpg'),
            HSP2:require('../assets/Images/hsproduct2.jpg'),
            HSP3:require('../assets/Images/hsproduct3.jpg'),
            HSP4:require('../assets/Images/hsproduct4.jpg'),
            HSPA1:require('../assets/Images/accessories1.jpg'),
            HSPA2:require('../assets/Images/accessories2.jpg'),
            HSPA3:require('../assets/Images/accessories3.jpg'),
            HSPA4:require('../assets/Images/accessories4.jpg'),
            OFB1:require('../assets/Images/brand1.png'),
            OFB2:require('../assets/Images/brand2.png'),
            OFB3:require('../assets/Images/brand3.png'),
            Banner:require('../assets/Images/banners.jpg'),
        }
        static dynamicStyle(
            pStyles: 
            ViewStyle
        |   ViewStyle[]
        |   TextStyle
        |   TextStyle[]
        |   ImageStyle
        |   ImageStyle[],
            lStyles:
            ViewStyle 
        |   ViewStyle[] 
        |   TextStyle 
        |   TextStyle[]
        |   ImageStyle 
        |   ImageStyle[],
            orientation:string 
            ){
        return   orientation === "portrait" ? pStyles:lStyles
    }

}      
     