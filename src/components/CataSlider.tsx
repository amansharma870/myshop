import React from 'react'
import { View,Text ,StyleSheet, ViewStyle, TextStyle, ImageStyle, TouchableOpacity, FlatList,Image} from "react-native"
import { Typography } from '../styles/Global';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Icon from './Icon';
import { SafeAreaView } from 'react-native-safe-area-context';
import { PrimaryTheme } from '../styles/Theme';


interface Props{
    onPress:any;
    subPlace?: any;
    subHeading?: any;
    overlayAlpha?: any;
    overlayColor?: any;
    contentContainerCustomStyle?:any;
    containerStyle?:ViewStyle |   ViewStyle[]
    |   TextStyle
    |   TextStyle[]
    |   ImageStyle
    |   ImageStyle[],
    SliderImage:any,
    icons:any
    categoryStyles?:ViewStyle |   ViewStyle[]
    |   TextStyle
    |   TextStyle[]
    |   ImageStyle
    |   ImageStyle[],
}

const CataSlider = (props:Props) => {



    return (
        <View style={{flexDirection: 'row', 
        flexWrap: 'wrap',marginBottom:12}}>
<FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={props.icons}
        renderItem={({ item, index }) => (
            <SafeAreaView style={{borderRadius:12,height:hp('13%'),width:wp('18.5%'),alignItems:'center'}} key={index}>

                   <TouchableOpacity onPress={props.onPress} activeOpacity={0.9} style={[[styles.categoryStyles, props.categoryStyles]]}>
                      <Icon size={38} color={PrimaryTheme.$MATERIAL_COLOR} name={item.name}/>
                       
                   </TouchableOpacity>
                   <View style={{alignItems:'center',paddingTop:hp('0.5%')}}>
                        
                        <Text style={{color:'#000'}}>{item.title}</Text>
                    </View>
                </SafeAreaView>
        )}
        keyExtractor={(item,index) => index.toString()}

      />
    </View>
    )
}
export default CataSlider;

const styles = StyleSheet.create({
    categoryStyles: {
    borderRadius:12,
    backgroundColor:'#c1c1c18f',
    padding:wp('3%')
},
})