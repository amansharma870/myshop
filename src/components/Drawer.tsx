import React from "react";
import {Text} from 'react-native'
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator, DrawerContent } from "@react-navigation/drawer";
import Profile from "../screens/Profile";
import Wishlist from "../screens/Wishlist";


const AppDrawer=()=>{
  const Drawer = createDrawerNavigator();


  return(
    <>
    <NavigationContainer>
      <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
          <Drawer.Screen name="HomeDrawer" component={Profile} />
          <Drawer.Screen name="SupportScreen" component={Wishlist} />
        </Drawer.Navigator>
    </NavigationContainer>
    </>
        )
}






export default AppDrawer;