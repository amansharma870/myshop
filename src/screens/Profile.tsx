import * as React from 'react'
import { useState } from 'react';
import {StyleSheet, Image, View, TouchableOpacity,Text, ScrollView, SafeAreaView} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import CustomButton from '../components/CustomButton';
import { PrimaryTheme } from '../styles/Theme';
import {widthPercentageToDP as wp,heightPercentageToDP as hp, heightPercentageToDP} from 'react-native-responsive-screen'
import Icon from '../components/Icon';
import { Typography } from '../styles/Global';
import Container from '../components/Container';

interface Props{
    route:any
    navigation:any
  }
 
const Profile =(props:Props)=>
    {
      const [image,setImage]=useState('../assets/Images/1.png');


      const GetfromCamera =()=>{
        ImagePicker.openCamera({
          width: 300,
          height: 300,
          cropping: true,
        }).then(image => {
          setImage(image.path)
        }).catch((err) => { console.log("openCamera catch" + err.toString()) });
      }
      const GetfromLibrary =()=>{
        ImagePicker.openPicker({
          width: 300,
          height: 400,
          cropping: true
        }).then(image => {
          console.log(image);
          setImage(image.path)
        }).catch((err) => { console.log("openCamera catch" + err.toString()) });
      }
        return(
          <ScrollView showsVerticalScrollIndicator={false}>
          <Container containerStyles={{paddingTop:12}}>
         
   
           <SafeAreaView>
           <SafeAreaView style={{flexDirection:"row"}}>
               <View
                 style={{
                   width:wp('38%')
                 }}>
                 <Image style={{backgroundColor:PrimaryTheme.$MATERIAL_COLOR,borderRadius:100,borderWidth:0,borderColor:PrimaryTheme.$MATERIAL_COLOR,marginLeft:wp('2%'),marginBottom:wp('2%')}} height={hp('18%')} width={wp('32%')} source={{uri:image}}/>  
                 <CustomButton onPress={GetfromCamera} textStyle={{color:'#fff'}} buttonStyle={styles.buttonStyleC} title={'Use Camera'}/>
                   <CustomButton onPress={GetfromLibrary} textStyle={{color:'#fff'}} buttonStyle={styles.buttonStyleC} title={'Gallary'}/>
                   
               </View>
               <View
                 style={{
                 }}>
                 <View style={{flexDirection:'row'}}>
                   <Text style={[Typography.heading,{fontSize:18,color:PrimaryTheme.$MATERIAL_COLOR}]}>Name: </Text><Text style={[Typography.heading,{fontSize:18}]}>Aman Sharma</Text>
                 </View>
                 <View style={{flexDirection:'row'}}>
                   <Text style={[Typography.heading,{fontSize:18,color:PrimaryTheme.$MATERIAL_COLOR}]}>City: </Text><Text style={[Typography.heading,{fontSize:18}]}>Abohar</Text>
                 </View>
                 <View style={{flexDirection:'row'}}>
                   <Text style={[Typography.heading,{fontSize:18,color:PrimaryTheme.$MATERIAL_COLOR}]}>Contact: </Text><Text style={[Typography.heading,{fontSize:18}]}>9115915870</Text>
                 </View>
                 <View style={{flexDirection:'row'}}>
                   <Text style={[Typography.heading,{fontSize:18,color:PrimaryTheme.$MATERIAL_COLOR}]}>Gmail: </Text><Text style={[Typography.heading,{fontSize:18}]}>aman@mail.com</Text>
                 </View>
               </View>
               
           </SafeAreaView>
              <SafeAreaView>
              <View style={{paddingTop:hp('3%')}}>
                 <Text style={[Typography.heading,{fontSize:22}]}>Account Settings</Text>
               </View>
               <TouchableOpacity style={{flexDirection:'row',justifyContent:'space-between'}}>
                 <Text style={[Typography.subheading,{fontSize:18}]}>Change Password</Text>
                 <Icon  name={'chevron-forward-outline'} size={18} style={{paddingRight:wp('3%')}}
                         color={'#686868'}
                   /> 
               </TouchableOpacity>
               <TouchableOpacity style={{flexDirection:'row',justifyContent:'space-between'}}>
                 <Text style={[Typography.subheading,{fontSize:18}]}>Notifications</Text>
                 <Icon  name={'chevron-forward-outline'} size={18} style={{paddingRight:wp('3%')}}
                         color={'#686868'}
                   /> 
               </TouchableOpacity>
               <TouchableOpacity style={{flexDirection:'row',justifyContent:'space-between'}}>
                 <Text style={[Typography.subheading,{fontSize:18}]}>Payment Options</Text>
                 <Icon  name={'chevron-forward-outline'} size={18} style={{paddingRight:wp('3%')}}
                         color={'#686868'}
                   /> 
               </TouchableOpacity>
              </SafeAreaView>
              <SafeAreaView>
              
               
               <TouchableOpacity style={{flexDirection:'row',paddingTop:hp('13%')}}>
               <Icon  name={'log-out-outline'} size={28} style={{paddingRight:wp('1%')}}
                         color={PrimaryTheme.$MATERIAL_COLOR}
                   /> 
                 <Text style={[Typography.subheading,{fontSize:18,color:PrimaryTheme.$MATERIAL_COLOR}]}>Sign Out</Text>
               </TouchableOpacity>
              </SafeAreaView>
           </SafeAreaView>
          </Container>
       </ScrollView>
        )
    }
export default Profile;

const styles = StyleSheet.create({
  buttonStyleC:{
    backgroundColor: PrimaryTheme.$MATERIAL_COLOR,
    borderWidth:2,
    borderColor:PrimaryTheme.$MATERIAL_COLOR,width:'90%',height:hp('5%'),
  },
  sectionStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    width:'100%',
    borderBottomWidth:1,
    borderBottomColor:PrimaryTheme.$MATERIAL_COLOR
   },
   sectionStyleTop: {
    flexDirection: 'row',
    alignItems: 'center',
    width:'100%',
    borderBottomWidth:1,
    borderBottomColor:PrimaryTheme.$MATERIAL_COLOR
   },
searchIcon: {
    padding: 10,
},
input: {
    paddingRight: 10,
    paddingLeft: 0,
    color: '#424242',
},
  });