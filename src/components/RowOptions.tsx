import * as React from 'react'
import {Button, Text, View} from 'react-native';
import { Typography } from '../styles/Global';
import { PrimaryTheme } from '../styles/Theme';
import CustomText from './CustomText';
import Icon from './Icon';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import { TouchableOpacity } from 'react-native-gesture-handler';

interface Props{
    rowTitle: any;
   
  }
const RowOpt =(props:Props)=>
    {
        return(
            <View style={{justifyContent:'space-between',flexDirection:'row',paddingTop:hp('1%')}}>
               <CustomText style={[Typography.heading]}>{props.rowTitle}</CustomText>
               <TouchableOpacity style={{flexDirection:'row'}}>
               <CustomText style={{color:PrimaryTheme.$MATERIAL_COLOR,fontWeight:'bold',fontSize:17}}>
                   See More 
               </CustomText>
               <Icon name={'chevron-forward'} size={22} color={PrimaryTheme.$MATERIAL_COLOR}/>
               </TouchableOpacity>
            </View>
        )
    }
export default RowOpt;