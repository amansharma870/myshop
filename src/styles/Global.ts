import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import { PrimaryTheme } from './Theme'

export const Typography : any = {
    title:{
      fontWeight:'bold',
      fontSize:wp('7%'),
      marginBottom: hp("3%"),
    },
    BottomName:{
      fontWeight:'bold',
      fontSize:wp('4%'),
      marginBottom:hp('0%')
    },
    heading:{
    fontSize:wp('5%'),
    color:PrimaryTheme.$DARK_COLOR,
    fontWeight:"bold",
    marginBottom: hp("2%"),
    fontFamily:'roboto'
    },
    subheading:{
    fontSize:wp('4.8%'),
    color:PrimaryTheme.$TEXT_COLOR,
    fontFamily:'Muli',
    fontWeight:"500",
    marginBottom: wp("3%")
    },
    paragraph:{
    fontSize:wp('4%'),
    color:PrimaryTheme.$TEXT_COLOR,
    fontFamily:'Muli',
    },
    BottomTitle:{
      fontSize:wp('3.5%'),
      color:PrimaryTheme.$DARK_COLOR,
      fontWeight:"bold",
    },
    BottomSubTitle:{
      fontSize:wp('3%'),
      color:PrimaryTheme.$DARK_COLOR,
      marginBottom: hp("1%"),
    },
    non_regularText:{
      fontSize:wp('4%'),
      color:PrimaryTheme.$TEXT_COLOR,
      fontFamily:'Muli',
      padding:7,
      fontWeight:"500",
    },
    errorText:{
    fontSize:wp('3.2%'),
    color:PrimaryTheme.$ERROR_COLOR,
    fontFamily:'Muli',
    },

    }
export const Spacing = {

    tiny: {
      marginLeft: wp('0.25%'),
      marginRight: wp('0.25%'),
      marginTop: hp('0.25%'),
      marginBottom: hp('0.25%'),
    },
    small: {
      marginLeft: wp('0.5%'),
      marginRight: wp('0.5%'),
      marginTop: hp('0.5%'),
      marginBottom: hp('0.5%'),
    },
    regular: {
      marginLeft: wp('1%'),
      marginRight: wp('1%'),
      marginTop: hp('1%'),
      marginBottom: hp('1%'),
    },
    large: {
      marginLeft: wp('1.5%'),
      marginRight: wp('1.5%'),
      marginTop: hp('1.5%'),
      marginBottom: hp('1.5%'),
    },
    extraLarge: {
      marginLeft: wp('2%'),
      marginRight: wp('2%'),
      marginTop: hp('2%'),
      marginBottom: hp('2%'),
    },
  };
export const Div_Area = {
    regular: {
      width:wp('90%'),
    },
    Phone_Text:{
      height:hp('3%'),
      padding:0,
      margin:0,
    },
    title_Image:{
      width:wp('60%'),
      height:hp('30%'),
      marginBottom:hp('2%')
    },
    
  };
export const Inputs_Area = {
    regular: {
      borderBottomWidth:1,
      width:wp('90%'),
      borderBottomColor:'#D8D8D8'
    },
    small: {
      width:wp('80%'),
      borderBottomColor:'#D8D8D8'
    },     
    large: {
      width:wp('96%'),
      borderRadius:6,
      backgroundColor:'#d8d5d4ed'
    },   
  };
export const wrapper = {
    regular: {
      alignItems:'center',
      flex:1,
      justifyContent:'center'
    },       
  };
