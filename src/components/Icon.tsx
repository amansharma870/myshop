import * as React from 'react';
import Icons from 'react-native-vector-icons/Ionicons';
import {utils} from '../utils/utils';

export interface Props {
    name: any;
    color?: string;
    size?: number;
    style?: any;
    noPrefix?: boolean;
    onPress?:any;
  }

  const Icon = (props: Props) => {
    const iconName = () => {
      if (utils.isIos) {
        return 'ios-' + props.name;
      }
      return 'md-' + props.name;
    };
    return (
      <Icons
        style={props.style}
        name={props.noPrefix ? props.name : iconName()}
        color={props.color}
        size={props.size}
        onPress={props.onPress}
      />
    );
  };
  Icon.defaultProps = {
    color: 'white',
    size: 35,
    noPrefix: false,
  };
  export default Icon;