export enum PrimaryTheme {
  $DARK_COLOR = '#000',
  $LIGHT_COLOR = '#fff',
  $TEXT_COLOR = '#333',
  $PRICE_COLOR = '#ff2323',
  $MATERIAL_COLOR = '#f39c12',
  $MATERIAL_SECONDARY = '#36454F',
  $FB_COLOR = '#3b5999',
  $TWI_COLOR = '#55acee',
  $LINKDN_COLOR = '#0077B5',
  $GPLUS_COLOR = '#dd4b39',
  $ERROR_COLOR = '#f9250b',
  $LIKE_COLOR = 'red',
  $MY_Theme = "$MY_Theme"
}