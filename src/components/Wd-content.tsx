import * as React from 'react'
import {StyleSheet, Image, View,Text} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import { SafeAreaView } from 'react-native-safe-area-context';
import { PrimaryTheme } from '../styles/Theme';
import Container from './Container';
import Icon from './Icon';

interface Props{
    wdImg:any
    productName:any
    productPrice:any
  }
const WDContent =(props:Props)=>
    {
        return(
                <TouchableOpacity activeOpacity={0.8} style={Pstyles.wdSection}>
                    <View style={{flexDirection:'row'}}>
                        <Image source={props.wdImg} style={[Pstyles.wdImg,props.wdImg]} />
                        <Icon size={35} name={'heart'} color={PrimaryTheme.$LIKE_COLOR}/>
                    </View>
                    <View style={{alignItems:'center'}}>
                        <Text style={Pstyles.wdText}>{props.productName}</Text>
                        <Text style={Pstyles.wdPrice}>{props.productPrice}</Text>
                    </View>
                </TouchableOpacity>
        )
    }
export default WDContent;


const Pstyles = StyleSheet.create({
    wdSection:{
        backgroundColor:PrimaryTheme.$MATERIAL_SECONDARY,
        padding:10,
        width:wp('45%'),
        borderRadius:18,
        marginBottom:hp('2%'),
        marginRight:11,
        flexWrap:'wrap'
    },
   wdImg:{
      width:wp('30%'),
      height:hp('18%'),
   },
   wdText:{
       color:'#fff',
       fontWeight:'bold',
       fontSize:16
   },wdPrice:{
    color:PrimaryTheme.$MATERIAL_COLOR,
    fontWeight:'bold',
    fontSize:20
   }
  });
  