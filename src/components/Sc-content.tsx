import * as React from 'react'
import { useState } from 'react';
import {StyleSheet, Image, View,Text,SafeAreaView,ScrollView, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp, heightPercentageToDP} from 'react-native-responsive-screen'
import { PrimaryTheme } from '../styles/Theme';
import Container from './Container';
import Icon from './Icon';
import { CheckBox } from 'react-native-elements';
import CustomButton from './CustomButton';
import CustomText from './CustomText';

interface Props{
    SCImg:any
    productName:any
    productPrice:any
  }
  
const SCContent =(props:Props)=>

    {     const [count, setCount] = useState(0);
        const [isSelected,setSelection]=useState(false)
        const Increment=()=>{
            if(count < 30 ){
                setCount(count + 1);
            }else{
                setCount(30)
            }
        }
        const Decrement=()=>{
            if(count > 0 ){
                setCount(count - 1);
            }else{
                setCount(0)
            }
        }
         
        return(
            
              <ScrollView>
                   <TouchableOpacity activeOpacity={1} style={{backgroundColor:PrimaryTheme.$MATERIAL_SECONDARY,marginBottom:hp('0.5%'),height:hp('13%'),padding:0}}>
                   <SafeAreaView style={{flexDirection:'row'}}>
                   <View>
                   <CheckBox
                            containerStyle={{paddingTop:hp('3%'),marginLeft:wp('2%'),padding:0,}}
                                size={30}
                                checkedColor={PrimaryTheme.$MATERIAL_COLOR}
                                checked={isSelected}
                                checkedIcon='dot-circle-o'
                                uncheckedIcon='circle-o'
                                onPress={()=>{setSelection(!isSelected)}}
                            />
                   </View>
                   <View>
                      <Image source={props.SCImg} style={[Pstyles.SCImg,props.SCImg]} />
                   </View>
                        <View style={{alignItems:'center',paddingTop:hp('2.5%'),paddingLeft:hp('2%')}}>
                                <Text style={Pstyles.SCText}>{props.productName}</Text>
                                <Text style={Pstyles.ScPrice}>{props.productPrice}</Text>
                            </View>
                        <View style={Pstyles.BtnSection}>
                            <CustomButton title={'+'} onPress={Increment} textStyle={{color:PrimaryTheme.$DARK_COLOR}} buttonStyle={Pstyles.BtnStyle}/>
                            <View>
                                <CustomText style={Pstyles.Quantity}>{count}</CustomText>
                            </View>
                            <CustomButton title={'-'} onPress={Decrement} textStyle={{color:PrimaryTheme.$DARK_COLOR}} buttonStyle={Pstyles.BtnStyle}/>
                        </View>
                   </SafeAreaView>
               </TouchableOpacity>
              </ScrollView>
        )
    }
export default SCContent;


const Pstyles = StyleSheet.create({
    BtnSection:{
        flexDirection:'row',
        marginTop:hp('2%'),
        backgroundColor:PrimaryTheme.$LIGHT_COLOR,
        marginLeft:wp('3%'),
        height:hp('6%'),
        width:wp('22%'),
        borderTopLeftRadius:hp('3%'),
        borderTopRightRadius:0,
        borderBottomLeftRadius:0,
        borderBottomRightRadius:hp('3%'),
    },
    BtnStyle:{
        width:wp('5%'),
        height:hp('4%'),
        backgroundColor:PrimaryTheme.$LIGHT_COLOR,
        borderTopLeftRadius:hp('2%'),
        borderTopRightRadius:0,
        borderBottomLeftRadius:0,
        borderBottomRightRadius:hp('2%'),
    },
    Quantity:{
        fontWeight:'bold',
        fontSize:20,
        paddingTop:hp('1%'),
        width:wp('8%'),
    },
    SCImg:{
      width:wp('13%'),
      flex:1,
      marginTop:hp('2%')
    },
   SCText:{
       color:'#fff',
       fontWeight:'bold',
       fontSize:13
   },ScPrice:{
    color:PrimaryTheme.$MATERIAL_COLOR,
    fontWeight:'bold',
    fontSize:17
   },

  });



  