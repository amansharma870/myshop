import * as React from 'react'
import {ImageStyle, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import Container from '../components/Container';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import { Typography } from '../styles/Global';
import { PrimaryTheme } from '../styles/Theme';
import CustomButton from '../components/CustomButton';
import ImageOverlay from "react-native-image-overlay";
import { utils } from '../utils/utils';


interface Props{
    need:string
    SliderImage?:ImageStyle | ImageStyle[]
    overlayColor?:any
    overlayAlpha?:number
}
const OfferCnt =(props:Props)=>
    {
        return(
            <>
            {props.need == 'BgColor' ?
            <SafeAreaView style={styles.section}>
            <Container containerStyles={{paddingTop:hp('4%'),paddingBottom:hp('4%')}}>
                <Text style={[Typography.title,{color:PrimaryTheme.$LIGHT_COLOR}]}>
                 SEE PRODUCT DISCOUNT UP TO 80%
                </Text>
                <Text style={[Typography.subheading,{color:PrimaryTheme.$LIGHT_COLOR}]}>
                 Lorem ipsum dolor sit amet.
                </Text>
                <CustomButton buttonStyle={{width:wp('30%')}} title={'SEE NOW'} onPress={{}}/>
            </Container>
        </SafeAreaView> :
        
        <ImageOverlay
        source={utils.Img.Banner}
        containerStyle={[styles.image,props.SliderImage]}
        overlayColor={props.overlayColor}
        overlayAlpha={props.overlayAlpha}
        />
        
        }        
            </>

        )
    }
export default OfferCnt;

const styles = StyleSheet.create({

    section: {
        backgroundColor:PrimaryTheme.$DARK_COLOR
    },
    image:{
        height:hp('50%'),borderRadius:9,width:wp('92%'),
    }
    })