import React from 'react'
import { View,Text ,StyleSheet, ViewStyle, TextStyle, ImageStyle, TouchableOpacity, FlatList,Image} from "react-native"
import { Typography } from '../styles/Global';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context';
import { PrimaryTheme } from '../styles/Theme';
import Icon from '../components/Icon';


interface Props{
    onPress?:any;
    categoryStyles?:ViewStyle |   ViewStyle[]
    |   TextStyle
    |   TextStyle[]
    |   ImageStyle
    |   ImageStyle[],
}

const FooterIcn = (props:Props) => {
    return (
        <SafeAreaView style={{flexDirection:'row',}}>
                    <TouchableOpacity activeOpacity={0.8} style={styles.IconBg}>
                       <Icon size={20} style={styles.FooterIcons} color={PrimaryTheme.$MATERIAL_SECONDARY} name={'logo-facebook'}/>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} style={styles.IconBg}>
                       <Icon size={20} style={styles.FooterIcons} color={PrimaryTheme.$MATERIAL_SECONDARY} name={'logo-twitter'}/>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} style={styles.IconBg}>
                       <Icon size={20} style={styles.FooterIcons} color={PrimaryTheme.$MATERIAL_SECONDARY} name={'logo-google'}/>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} style={styles.IconBg}>
                       <Icon size={20} style={styles.FooterIcons} color={PrimaryTheme.$MATERIAL_SECONDARY} name={'logo-instagram'}/>
                    </TouchableOpacity>
                  </SafeAreaView>
    )
}
export default FooterIcn;

const styles = StyleSheet.create({
    FooterIcons: {
    padding:wp('3%')
},
IconBg:{
    backgroundColor:PrimaryTheme.$LIGHT_COLOR,
    marginRight:wp('4%'),
    borderRadius:22
}
})