import React from 'react'
import { View,Text , ViewStyle, TextStyle, ImageStyle,StyleSheet} from "react-native"
import { Typography } from '../styles/Global';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context';
import { PrimaryTheme } from '../styles/Theme';
import FooterIcn from './FooterIcn';
import Container from '../components/Container';


interface Props{
    onPress?:any;
    categoryStyles?:ViewStyle |   ViewStyle[]
    |   TextStyle
    |   TextStyle[]
    |   ImageStyle
    |   ImageStyle[],
}

const Footer = (props:Props) => {



    return (
        <SafeAreaView style={styles.FooterSec}>
           <Container containerStyles={{alignItems:'center'}}>
               <Text style={[Typography.title,{padding:wp('3%'),color:PrimaryTheme.$LIGHT_COLOR}]}>
                   MSHOP
               </Text>
               <FooterIcn />
                  <View style={{borderBottomWidth:1,borderBottomColor:PrimaryTheme.$LIGHT_COLOR,width:'100%',paddingTop:hp('3%')}}></View>
               <Text style={[Typography.BottomTitle,{padding:wp('3%'),color:PrimaryTheme.$MATERIAL_SECONDARY}]}>
                 Copyright © All Right Reserved
               </Text>
           </Container>
        </SafeAreaView>
    )
}
export default Footer;

const styles = StyleSheet.create({
    FooterSec: {
        backgroundColor:PrimaryTheme.$MATERIAL_COLOR,
        paddingTop:hp('5%'),
        paddingBottom:hp('1%'),
        borderTopLeftRadius:wp('5%'),
        borderTopRightRadius:wp('5%')
},
})