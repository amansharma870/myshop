import * as React from 'react'
import {ScrollView, Text, View} from 'react-native';
import Container from '../components/Container';
import WDContent from '../components/Wd-content';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import { utils } from '../utils/utils';
import { Typography } from '../styles/Global';
import OfferCnt from '../components/Offers';


const Wishlist =()=>
    {
        return(
        <ScrollView showsVerticalScrollIndicator={false}>
          <OfferCnt need={'BgColor'}/>
            <Container>
            
              <View style={{flexDirection:'row'}}>
                  <WDContent 
                    wdImg={utils.Img.WDP1}
                    productName={'Elegant Blue t-Shirt'}
                    productPrice={'$80.00'}/>
                  <WDContent 
                    wdImg={utils.Img.WDP2}
                    productName={'Elegant Blue t-Shirt'}
                    productPrice={'$180.00'}/>
              </View>
              <View style={{flexDirection:'row'}}>
                  <WDContent 
                    wdImg={utils.Img.WDP3}
                    productName={'Elegant Blue t-Shirt'}
                    productPrice={'$810.00'}/>
                  <WDContent 
                    wdImg={utils.Img.WDP4}
                    productName={'Elegant Blue t-Shirt'}
                    productPrice={'$20.00'}/>
              </View>
              <View style={{flexDirection:'row'}}>
                  <WDContent 
                    wdImg={utils.Img.WDP5}
                    productName={'Elegant Blue t-Shirt'}
                    productPrice={'$30.00'}/>
                  <WDContent 
                    wdImg={utils.Img.WDP6}
                    productName={'Elegant Blue t-Shirt'}
                    productPrice={'$40.00'}/>
              </View>
            </Container>
        </ScrollView>
        )
    }
export default Wishlist;