import React from 'react'
import { View,Text ,StyleSheet, ViewStyle, TextStyle, Image, FlatList,TouchableOpacity, ImageStyle} from "react-native"
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import { SafeAreaView } from 'react-native-safe-area-context'
import RowOpt from './RowOptions'



interface Props{
    onPress?: any;
    style?: ImageStyle | ImageStyle[];
    source:any
}

const SimSlider = (props:Props) => {
    return (
       <>
 <RowOpt rowTitle={'The Best Accessories'}/>
        <SafeAreaView style={{flexDirection: 'row', 
        flexWrap: 'wrap',marginBottom:hp('2%')}}>
<FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={props.source}
        renderItem={({ item, index }) => (
            

               
               <TouchableOpacity onPress={props.onPress} activeOpacity={0.9} style={styles.section} key={index}>
               <Image
                source={item.image}
                style={[styles.image,props.style]}
                />
                
            </TouchableOpacity>
            
        )}
        keyExtractor={(item,index) => index.toString()}

      />
    </SafeAreaView>
       </>
    )
}
export default SimSlider;

const styles = StyleSheet.create({

section: {
    flexDirection: 'row', 
    flexWrap: 'wrap',paddingRight:wp('3%'),
    
},
image:{
    height:hp('25%'),width:wp('40%'),borderRadius:15
}
})