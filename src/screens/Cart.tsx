import * as React from 'react'
import {Button, Text, View,ScrollView} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import Container from '../components/Container';
import CorText from '../components/CornerText';
import CustomButton from '../components/CustomButton';
import SCContent from '../components/Sc-content';
import { utils } from '../utils/utils';

interface Props{
    route:any
    navigation:any
  }
const Cart =(props:Props)=>
    {
        return(
            <ScrollView showsVerticalScrollIndicator={false}>
                <Container>
                     
                        <SCContent 
                        SCImg={utils.Img.WDP1}
                        productName={'Elegant Blue t-Shirt'}
                        productPrice={'$80.00'}/>
                        <SCContent 
                        SCImg={utils.Img.WDP2}
                        productName={'Elegant Blue t-Shirt'}
                        productPrice={'$80.00'}/>
                        <SCContent 
                        SCImg={utils.Img.WDP3}
                        productName={'Elegant Blue t-Shirt'}
                        productPrice={'$80.00'}/>
                         <SCContent 
                        SCImg={utils.Img.WDP4}
                        productName={'Elegant Blue t-Shirt'}
                        productPrice={'$80.00'}/>
                        <SCContent 
                        SCImg={utils.Img.WDP5}
                        productName={'Elegant Blue t-Shirt'}
                        productPrice={'$80.00'}/>
                
                    <CorText />
                    <CustomButton title={'Checkout'} onPress={{}}/>
                    
                </Container>
                    
            </ScrollView>
        )
    }
export default Cart;