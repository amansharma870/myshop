import * as React from 'react'
import {Button, ScrollView, StyleSheet, View} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import BrandSlider from '../components/BrandSlider';
import Container from '../components/Container';
import OfferCnt from '../components/Offers';
import { MainSlider } from '../styles/SliderData';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import Wishlist from './Wishlist';
import RowOpt from '../components/RowOptions';

interface Props{
    route:any
    navigation:any
  }
const OfficalBrand =(props:Props)=>
    {
        return(
          <SafeAreaView>
          <ScrollView showsVerticalScrollIndicator={false}>
          
          <Container containerStyles={{paddingBottom:0}}>
            <View>
              <BrandSlider source={MainSlider[5]}/>
               <OfferCnt need={'Image'}/>
            </View>
            <RowOpt rowTitle={'All Product Brands'}/>
          </Container>
          <Wishlist />
          </ScrollView>
      </SafeAreaView>
        )
    }
export default OfficalBrand;

