import * as React from 'react'
import {Button, Text, View} from 'react-native';
import { PrimaryTheme } from '../styles/Theme';
import CustomText from './CustomText';

interface Props{
   
  }
const CorText =(props:Props)=>
    {
        return(
            <View style={{justifyContent:'space-between',flexDirection:'row'}}>
               <CustomText style={{color:PrimaryTheme.$MATERIAL_SECONDARY,fontWeight:'bold',fontSize:17}}>Total</CustomText>
               <CustomText style={{color:PrimaryTheme.$MATERIAL_COLOR,fontWeight:'bold',fontSize:17}}>$400</CustomText>
            </View>
        )
    }
export default CorText;