import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import { CardStyleInterpolators, createStackNavigator } from '@react-navigation/stack';
import {View,Text} from 'react-native';
import { PrimaryTheme } from '../styles/Theme';
import Home from './Home';
import WishList from './Wishlist';
import OfficalBrand from './OfficialBrands';
import Cart from './Cart';
import Profile from './Profile';
import Icon from '../components/Icon';
import { createDrawerNavigator, DrawerContent } from '@react-navigation/drawer';
import { Typography } from '../styles/Global';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import Wishlist from './Wishlist';

interface Props{
    route:any
    navigation:any
  }


export enum ScreenNames {
    HOMESCREEN = 'Home',
    WISHSCREEN= 'WishList',
    OFFSCREEN = 'Offical Brand',
    CARTSCREEN = 'Shopping Cart',
    PROFILE= 'Profile',
    DRAWER= 'Drawer'
  }
  const HomeStack = createStackNavigator();
const WishlistStack = createStackNavigator();
const OffBrandStack = createStackNavigator();
const CartStack = createStackNavigator();
const ProfileStack = createStackNavigator();






function LogoTitle() {
  return (
    <View style={{marginTop:heightPercentageToDP('3%')}}>
      <Text style={[Typography.title,{color:PrimaryTheme.$MATERIAL_COLOR}]}>M<Text style={[Typography.title,{color:PrimaryTheme.$MATERIAL_SECONDARY}]}>Shop</Text></Text>
    </View>
  );
}
function HomeStackScreen(props:Props) {
    return (
        <HomeStack.Navigator  screenOptions={{ 
            headerStyle: {
                elevation: 0,
                shadowOpacity: 0,
              },
              
              headerLeft:()=>(
                <View style={{flexDirection:'row'}}>
                 <Icon size={35} onPress={props.navigation.openDrawer} name={'menu'} color={PrimaryTheme.$MATERIAL_SECONDARY}/>
              </View>
              ),
              headerRight:()=>(
                <View style={{flexDirection:'row'}}>
                 <Icon size={25} name={'notifications'} color={PrimaryTheme.$MATERIAL_SECONDARY}/>
              </View>
              
              ),
          }}
          
        >
          <HomeStack.Screen options={{ headerTitle: () => <LogoTitle /> }}  name="Home" component={Home} />
        </HomeStack.Navigator>
    );
  }
  function WishStackScreen() {
    return (
        <WishlistStack.Navigator   screenOptions={{ 
          headerShown: true, headerTitleAlign: 'center', 
          headerTitleStyle:{
            backgroundColor:PrimaryTheme.$LIGHT_COLOR,
            color:PrimaryTheme.$MATERIAL_SECONDARY,
            fontSize:25,
            
        },
        
          }
        }
        >
          <WishlistStack.Screen   name="Wishlist" component={WishList} />
        </WishlistStack.Navigator>
    );
  }
  function OffStackScreen() {
    return (
        <OffBrandStack.Navigator  screenOptions={{ 
          headerShown: true, headerTitleAlign: 'center', 
          headerTitleStyle:{
            backgroundColor:PrimaryTheme.$LIGHT_COLOR,
            color:PrimaryTheme.$MATERIAL_SECONDARY,
            fontSize:25
        },
          }}
        >
          <OffBrandStack.Screen  name="Official Brands" component={OfficalBrand} />
        </OffBrandStack.Navigator>
    );
  }
  function CartStackScreen() {
    return (
        <CartStack.Navigator  screenOptions={{ 
          headerShown: true, headerTitleAlign: 'center', 
          headerTitleStyle:{
            backgroundColor:PrimaryTheme.$LIGHT_COLOR,
            color:PrimaryTheme.$MATERIAL_SECONDARY,
            fontSize:25
        },
          }}
        >
          <CartStack.Screen  name="Shopping Cart" component={Cart} />
        </CartStack.Navigator>
    );
  }
  function ProfileStackScreen() {
    return (
        <ProfileStack.Navigator  screenOptions={{ 
          headerShown: true, headerTitleAlign: 'center', 
          headerTitleStyle:{
            backgroundColor:PrimaryTheme.$LIGHT_COLOR,
            color:PrimaryTheme.$MATERIAL_SECONDARY,
            fontSize:25
        },
          }}
        >
          <ProfileStack.Screen  name="Profile" component={Profile} />
        </ProfileStack.Navigator>
    );
  }
  const Tab = createBottomTabNavigator();
  const TabData =()=>{
      return(
          <>
            <Tab.Navigator 
            tabBarOptions={{
                activeBackgroundColor:PrimaryTheme.$MATERIAL_SECONDARY,
                activeTintColor:PrimaryTheme.$MATERIAL_COLOR,
            }}
              initialRouteName={'Home'}
            >
                <Tab.Screen options={{
                    tabBarIcon:({})=>(
                        <View>
                           <Icon name={'home'}
                                 style={[{color:PrimaryTheme.$MATERIAL_COLOR}]} 
                                 size={25}
                                 />
                        </View>
                    ),
                    
                }} name="Home" component={HomeStackScreen} />
                <Tab.Screen options={{
                    
                    tabBarIcon:({})=>(
                        <View>
                           <Icon name={'bookmarks'}
                                 style={[{color:PrimaryTheme.$MATERIAL_COLOR}]} 
                                 size={25}
                                 />
                        </View>
                    ),
                }} name="WishList" component={WishStackScreen}  />
                <Tab.Screen options={{
                    tabBarIcon:({})=>(
                        <View>
                           <Icon name={'library'}
                                 style={[{color:PrimaryTheme.$MATERIAL_COLOR}]} 
                                 size={25}
                                 />
                        </View>
                    ),
                }} name="Offical Brand" component={OffStackScreen}  />
                <Tab.Screen options={{
                    tabBarIcon:({})=>(
                        <View>
                           <Icon name={'cart'}
                                 style={[{color:PrimaryTheme.$MATERIAL_COLOR}]} 
                                 size={25}
                                 />
                        </View>
                    ),
                    tabBarBadge:"5"
                }} name="Cart" component={CartStackScreen}  />
                <Tab.Screen options={{
                    tabBarIcon:({})=>(
                        <View>
                           <Icon name={'happy'}
                                 style={[{color:PrimaryTheme.$MATERIAL_COLOR}]} 
                                 size={25}
                                 />
                        </View>
                    ),
                    tabBarBadgeStyle:{backgroundColor:'red'}                    
                }} name="Profile" component={ProfileStackScreen}  />
                
            </Tab.Navigator>
          </>
      )
  }
  export default TabData;